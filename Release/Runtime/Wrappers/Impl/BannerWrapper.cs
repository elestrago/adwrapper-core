using Zenject;

namespace Libs.AdWrapper.Wrappers.Impl
{
	public class BannerWrapper : AbstractAdWrapper, IBannerWrapper
	{
		private readonly IBannerManager _adManager;

		protected BannerWrapper(IBannerManager adManager, SignalBus signalBus) 
			: base(adManager, signalBus)
		{
			_adManager = adManager;
		}

		public void Hide() => _adManager.Hide();
	}
}