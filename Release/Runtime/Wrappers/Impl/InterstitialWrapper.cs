using Zenject;

namespace Libs.AdWrapper.Wrappers.Impl
{
	public class InterstitialWrapper : AbstractAdWrapper, IInterstitialWrapper
	{
		protected InterstitialWrapper(IInterstitialManager adManager, SignalBus signalBus) 
			: base(adManager, signalBus)
		{
		}
	}
}