using Libs.AdWrapper.Mrec;
using Zenject;

namespace Libs.AdWrapper.Wrappers.Impl
{
    public class MrecWrapper : AbstractAdWrapper, IMrecWrapper
    {
        protected MrecWrapper(IMrecManager adManager, SignalBus signalBus) 
            : base(adManager, signalBus)
        {
        }
    }
}