using System;
using Libs.AdWrapper.Impl;
using Libs.AdWrapper.Models;
using Libs.AdWrapper.Signals;
using PdUtils;
using UniRx;
using UnityEngine;
using Zenject;

namespace Libs.AdWrapper.Wrappers
{
	public abstract class AbstractAdWrapper : IAdWrapper
	{
		private readonly IAdManager _adManager;
		private readonly SignalBus _signalBus;

		private readonly string _className;

		private IAdRequest _adRequest;
		private bool _isWaitPreload;
		private bool _isLoaded;
		private DateTime _dateTime;

		private bool _receivedCompleted;
		private bool _showImmediately;
		private bool _showing;

		private Action<EAdState, IAdRequest> _changeStateAction;

		private bool _configured;

		private IDisposable _preloadDisposable;

		protected AbstractAdWrapper(IAdManager adManager, SignalBus signalBus)
		{
			_adManager = adManager;
			_signalBus = signalBus;
			_className = GetType().Name;
		}

		public virtual void Configure()
		{
			if (_configured)
				return;
			_configured = true;

			_adManager.Configure();
			_adManager.ListenState(OnChangeState);
			Debug.Log($"[{_className}] Configured");
		}

		public void SetChangeStateAction(Action<EAdState, IAdRequest> changeStateAction) =>
			_changeStateAction = changeStateAction;

		public bool IsLoaded(IAdRequest adRequest)
		{
			_adRequest = adRequest;
			return _adManager.IsLoaded(adRequest);
		}

		public bool IsAdsForbidden => _adManager.IsAdsForbidden;

		public void RequestAllowAds(IAdRequest request, Action success) => _adManager.RequestAllow(request, success);

		public void PreloadOnly()
		{
			if (!_adManager.PreloadAutomatically)
				return;
			InternalPreload(AdRequest.Empty, false);
		}

		public void Preload(IAdRequest adRequest) => InternalPreload(adRequest, true);

		private void InternalPreload(IAdRequest adRequest, bool showImmediately)
		{
			Debug.Log($"[{_className}] InternalPreload on place -> " + adRequest?.GetPlace() + ", _isLoaded: " +
			          _isLoaded + ", _isWaitPreload: " + _isWaitPreload
			          + ", showImmediately: " + showImmediately);
			_showImmediately = showImmediately;
			_adRequest = adRequest;
			if (_isLoaded || _isWaitPreload)
			{
				Debug.Log($"[{_className}] InternalPreload on place " + adRequest?.GetPlace() +
				          ", _isLoaded and _isWaitPreload should be false");
				return;
			}

			_isWaitPreload = true;
			_adManager.Preload(adRequest);
		}

		public void Show(IAdRequest adRequest)
		{
			Debug.Log($"[{_className}] Show on place " + adRequest.GetPlace() + ", _isLoaded: " + _isLoaded +
			          ", _showing: " + _showing);
			if (_showing)
			{
				Debug.Log($"[{_className}] Show on place " + adRequest.GetPlace() + ", _showing should be false");
				return;
			}

			_showImmediately = true;
			_adRequest = adRequest;
			if (!_isLoaded)
			{
				Debug.Log($"[{_className}] Show on place " + adRequest.GetPlace() + ", _isLoaded should be true");
				return;
			}

			_receivedCompleted = false;
			_isLoaded = false;
			_showing = true;
			_dateTime = DateTime.UtcNow;
			_adManager.Show(adRequest);
		}

		private void OnChangeState(EAdState state, string error, IAdResponse adResponse)
		{
			Debug.Log($"[{_className}] OnChangeState " + state + ", _showImmediately: " + _showImmediately);
			if (_showImmediately)
			{
				HandleStateChangeImmediatelyShow(state, error, adResponse);
			}
			else
			{
				HandleStateChangeDelayedShow(state, error);
			}
		}

		private void HandleStateChangeDelayedShow(EAdState state, string error)
		{
			Debug.Log($"[{_className}] HandleStateChangeDelayedShow: " + state + ", error: " + error);

			if (state != EAdState.Preload)
			{
				_isWaitPreload = false;
			}

			if (state == EAdState.Loaded)
			{
				_isLoaded = true;
			}

			if (state == EAdState.Fail)
			{
				_isLoaded = false;
				if (_adManager.PreloadAutomatically && _preloadDisposable == null)
				{
					_preloadDisposable = Observable.Timer(TimeSpan.FromSeconds(2)).Subscribe(_ =>
					{
						_preloadDisposable = null;
						InternalPreload(_adRequest, false);
					});
				}
			}
		}

		private void HandleStateChangeImmediatelyShow(EAdState state, string error, IAdResponse adResponse)
		{
			Debug.Log($"[{_className}] HandleStateChangeImmediately: " + state + ", error: " + error);

			switch (state)
			{
				case EAdState.None:
					break;
				case EAdState.Open:
					HandleOpen(adResponse);
					break;
				case EAdState.Preload:
					HandlePreload(adResponse);
					break;
				case EAdState.Fail:
					HandleFail(adResponse, error);
					break;
				case EAdState.Forbidden:
					HandleForbidden(adResponse);
					break;
				case EAdState.Loaded:
					HandleLoaded(adResponse);
					break;
				case EAdState.Complete:
					HandleComplete(adResponse);
					break;
				case EAdState.Close:
					HandleClose(adResponse);
					break;
				case EAdState.FailToPlay:
					HandleFailToPlay(adResponse, error);
					break;
				case EAdState.Expire:
					HandleExpire(adResponse);
					break;
				case EAdState.Clicked:
					HandleClick(adResponse);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(state), state, null);
			}
		}

		private void OnFailed(Action<bool> fireSignalAction)
		{
			NetworkConnectionChecker.Check(() => { fireSignalAction?.Invoke(false); },
				() => { fireSignalAction?.Invoke(true); });
		}

		private void HandleOpen(IAdResponse adResponse)
		{
			_adRequest?.GetListener()?.OpenListener?.Handle();
			HandleChangeStates(EAdState.Open);
			var signal = CreateSignal(EAdState.Open, adResponse);
			_signalBus.Fire(signal);
		}

		private void HandlePreload(IAdResponse adResponse)
		{
			_dateTime = DateTime.UtcNow;
			_adRequest?.GetListener()?.PreloadListener?.Handle();
			HandleChangeStates(EAdState.Preload);
			var signal = CreateSignal(EAdState.Preload, adResponse);
			_signalBus.Fire(signal);
		}

		private void HandleFail(IAdResponse adResponse, string error)
		{
			_isWaitPreload = false;
			_isLoaded = false;
			OnFailed(networkOrHttpError =>
			{
				var duration = (DateTime.UtcNow - _dateTime).TotalSeconds;
				_adRequest?.GetListener()?.FailListener?.Handle(networkOrHttpError);
				HandleChangeStates(EAdState.Fail);
				var signal = CreateSignal(EAdState.Fail, adResponse, duration);
				signal.Error = error;
				signal.NetworkOrHttpError = networkOrHttpError;
				_signalBus.Fire(signal);
				_adRequest = null;
			});
			if (_adManager.PreloadAutomatically)
			{
				InternalPreload(_adRequest, false);
			}
		}

		private void HandleFailToPlay(IAdResponse adResponse, string error)
		{
			_isWaitPreload = false;
			_isLoaded = false;
			OnFailed(networkOrHttpError =>
			{
				var duration = (DateTime.UtcNow - _dateTime).TotalSeconds;
				_adRequest?.GetListener()?.FailToPlayListener?.Handle(networkOrHttpError);
				var signal = CreateSignal(EAdState.FailToPlay, adResponse, duration);
				HandleChangeStates(EAdState.FailToPlay);
				signal.Error = error;
				signal.NetworkOrHttpError = networkOrHttpError;
				_signalBus.Fire(signal);
				_adRequest = null;
			});
			if (_adManager.PreloadAutomatically)
			{
				InternalPreload(_adRequest, false);
			}
		}

		private void HandleForbidden(IAdResponse adResponse)
		{
			_isWaitPreload = false;
			_isLoaded = false;
			var duration = (DateTime.UtcNow - _dateTime).TotalSeconds;
			_adRequest?.GetListener()?.ForbiddenListener?.Handle();
			HandleChangeStates(EAdState.Forbidden);
			var signal = CreateSignal(EAdState.Forbidden, adResponse, duration);
			_signalBus.Fire(signal);
			_adRequest = null;
		}

		private void HandleLoaded(IAdResponse adResponse)
		{
			_isWaitPreload = false;
			_isLoaded = true;
			var duration = (DateTime.UtcNow - _dateTime).TotalSeconds;
			_adRequest?.GetListener()?.LoadedListener?.Handle();
			HandleChangeStates(EAdState.Loaded);
			var signal = CreateSignal(EAdState.Loaded, adResponse, duration);
			_signalBus.Fire(signal);
		}

		private void HandleComplete(IAdResponse adResponse)
		{
			_receivedCompleted = true;
			_isWaitPreload = false;
			_isLoaded = false;
			_showing = false;
			var duration = (DateTime.UtcNow - _dateTime).TotalSeconds;
			_adRequest?.GetListener()?.CompleteListener?.Handle();
			HandleChangeStates(EAdState.Complete);
			var signal = CreateSignal(EAdState.Complete, adResponse, duration);
			_signalBus.Fire(signal);
		}

		private void HandleClick(IAdResponse adResponse)
		{
			_isWaitPreload = false;
			_isLoaded = false;
			var signal = CreateSignal(EAdState.Clicked, adResponse);
			_signalBus.Fire(signal);
		}

		private void HandleClose(IAdResponse adResponse)
		{
			_isWaitPreload = false;
			_isLoaded = false;
			_showing = false;
			var duration = (DateTime.UtcNow - _dateTime).TotalSeconds;
			if (!_receivedCompleted) // send close only if no completed
			{
				_adRequest?.GetListener()?.CloseListener?.Handle();
			}

			HandleChangeStates(EAdState.Close);
			var signal = CreateSignal(EAdState.Close, adResponse, duration);
			_signalBus.Fire(signal);
			if (_adManager.PreloadAutomatically)
			{
				InternalPreload(_adRequest, false);
			}

			_adRequest = null;
		}

		private void HandleExpire(IAdResponse adResponse)
		{
			var signal = CreateSignal(EAdState.Expire, adResponse);
			_signalBus.Fire(signal);
			if (_adManager.PreloadAutomatically)
			{
				InternalPreload(_adRequest, false);
			}

			_adRequest = null;
		}

		public void Reset()
		{
			_isLoaded = false;
			_showImmediately = false;
			_isWaitPreload = false;
			_showing = false;
		}

		private void HandleChangeStates(EAdState adState)
		{
			_adRequest?.GetListener()?.StateChangedListener?.HandleChangeState(adState, _adRequest);
			_changeStateAction?.Invoke(adState, _adRequest);
		}

		private SignalAd CreateSignal(EAdState adState, IAdResponse adResponse, double? duration = null)
		{
			var sdkType = _adManager.GetSdkType();
			var adType = _adManager.GetAdType();
			return new SignalAd(_adRequest?.GetPlace(), adState, sdkType, adType, adResponse, duration);
		}
	}
}