namespace Libs.AdWrapper.StateListeners
{
	public interface IAdForbiddenListener : IStateListener
	{
		void Handle();
	}
}