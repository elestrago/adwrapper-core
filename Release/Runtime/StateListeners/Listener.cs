namespace Libs.AdWrapper.StateListeners
{

	public class Listener : IStateListener
	{
		public IAdPreloadListener PreloadListener { get; private set; }
		public IAdFailListener FailListener { get; private set; }
		public IAdFailToPlayListener FailToPlayListener { get; private set; }
		public IAdLoadedListener LoadedListener { get; private set; }
		public IAdOpenListener OpenListener { get; private set; }
		public IAdCloseListener CloseListener { get; private set; }
		public IAdForbiddenListener ForbiddenListener { get; private set; }
		public IAdCompleteListener CompleteListener { get; private set; }
		public IAdStateChangedListener StateChangedListener { get; private set; }

		private Listener()
		{
			
		}

		public static Listener Create(IStateListener obj)
		{
			var listener = new Listener();
			if(obj is IAdPreloadListener preloadListener)
			{
				listener.PreloadListener = preloadListener;
			}
			if(obj is IAdFailListener failListener)
			{
				listener.FailListener = failListener;
			}
			if(obj is IAdFailToPlayListener failToPlayListener)
			{
				listener.FailToPlayListener = failToPlayListener;
			}
			if(obj is IAdLoadedListener loadedListener)
			{
				listener.LoadedListener = loadedListener;
			}
			if(obj is IAdOpenListener openListener)
			{
				listener.OpenListener = openListener;
			}
			if(obj is IAdCloseListener closeListener)
			{
				listener.CloseListener = closeListener;
			}
			if(obj is IAdForbiddenListener forbiddenListener)
			{
				listener.ForbiddenListener = forbiddenListener;
			}
			if(obj is IAdCompleteListener completeListener)
			{
				listener.CompleteListener = completeListener;
			}
			if(obj is IAdStateChangedListener stateChangedListener)
			{
				listener.StateChangedListener = stateChangedListener;
			}
			return listener;
		} 
	}
}