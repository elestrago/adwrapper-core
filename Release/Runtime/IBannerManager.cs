namespace Libs.AdWrapper
{
    public interface IBannerManager : IAdManager
    {
        void Hide();
    }
}