using Libs.AdWrapper.Models;
using UnityEngine;

namespace Libs.AdWrapper.Impl.Emulator
{
	[CreateAssetMenu(menuName = "Settings/AdManagerEmulatorSettings", fileName = "AdManagerEmulatorSettings")]
	public class AdManagerEmulatorSettings : ScriptableObject, IAdEmulatorSettings
	{
		[SerializeField] private EAdState adState;
		[SerializeField] private EAdType adType;
		[SerializeField] private float delayTimeS;
		[SerializeField] private int loadFailsInRow;
		[SerializeField] private bool preloadAutomatically;
		

		public EAdState AdState => adState;
		public EAdType AdType => adType;
		public float DelayTimeS => delayTimeS;
		public int LoadFailsInRow => loadFailsInRow;
		public bool PreloadAutomatically => preloadAutomatically;
	}
}