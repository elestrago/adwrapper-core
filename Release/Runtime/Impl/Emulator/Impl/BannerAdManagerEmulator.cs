namespace Libs.AdWrapper.Impl.Emulator.Impl
{
    public class BannerAdManagerEmulator : AdManagerEmulator, IBannerManager
    {
        public BannerAdManagerEmulator(IAdEmulatorSettings emulatorSettings) : base(emulatorSettings)
        {
        }
    }
}