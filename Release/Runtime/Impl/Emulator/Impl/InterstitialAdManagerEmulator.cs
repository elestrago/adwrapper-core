namespace Libs.AdWrapper.Impl.Emulator.Impl
{
    public class InterstitialAdManagerEmulator : AdManagerEmulator, IInterstitialManager
    {
        public InterstitialAdManagerEmulator(IAdEmulatorSettings emulatorSettings) : base(emulatorSettings)
        {
        }
    }
}