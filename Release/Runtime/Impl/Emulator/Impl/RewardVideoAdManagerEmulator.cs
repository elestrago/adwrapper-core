namespace Libs.AdWrapper.Impl.Emulator.Impl
{
    public class RewardVideoAdManagerEmulator : AdManagerEmulator, IRewardVideoManager
    {
        public RewardVideoAdManagerEmulator(IAdEmulatorSettings emulatorSettings) : base(emulatorSettings)
        {
        }
    }
}