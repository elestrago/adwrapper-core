namespace Libs.AdWrapper.Models
{
	public enum EAdState
	{
		None,
		Preload,
		Fail,
		FailToPlay,
		Loaded,
		Open,
		Close,
		Complete,
		Clicked,
		Forbidden,
		Expire
	}
}