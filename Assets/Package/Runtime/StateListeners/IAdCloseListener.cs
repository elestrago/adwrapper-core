namespace Libs.AdWrapper.StateListeners
{
	public interface IAdCloseListener : IStateListener
	{
		void Handle();
	}
}