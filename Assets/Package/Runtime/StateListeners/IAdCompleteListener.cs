namespace Libs.AdWrapper.StateListeners
{
	public interface IAdCompleteListener : IStateListener
	{
		void Handle();
	}
}