namespace Libs.AdWrapper.Wrappers
{
	public interface IBannerWrapper : IAdWrapper
	{
		void Hide();
	}
}