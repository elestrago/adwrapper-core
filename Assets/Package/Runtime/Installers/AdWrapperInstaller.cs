using Libs.AdWrapper.Impl.Emulator.Impl;
using Libs.AdWrapper.Mrec;
using Libs.AdWrapper.Signals;
using Libs.AdWrapper.Wrappers;
using Libs.AdWrapper.Wrappers.Impl;
using Zenject;

namespace Libs.AdWrapper.Installers
{
    public class AdWrapperInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.DeclareSignal<SignalAd>();
#if UNITY_EDITOR
            Container.BindInterfacesTo<BannerAdManagerEmulator>().AsSingle();
            Container.BindInterfacesTo<InterstitialAdManagerEmulator>().AsSingle();
            Container.BindInterfacesTo<RewardVideoAdManagerEmulator>().AsSingle();
            Container.BindInterfacesTo<MrecAdManagerEmulator>().AsSingle();
#endif
	        Container.Bind<IInterstitialWrapper>().To<InterstitialWrapper>().AsSingle();
            Container.Bind<IRewardVideoWrapper>().To<RewardVideoWrapper>().AsSingle();
            Container.Bind<IBannerWrapper>().To<BannerWrapper>().AsSingle();
            Container.Bind<IMrecWrapper>().To<MrecWrapper>().AsSingle();
        }
    }
}