using System;
using Libs.AdWrapper.Impl;
using Libs.AdWrapper.Models;

namespace Libs.AdWrapper
{
	public interface IAdManager : IAdBasic
	{
		void Configure();
		
		bool PreloadAutomatically { get; }
		
		void ListenState(Action<EAdState, string, IAdResponse> state);
		
		ESdkType GetSdkType();
		
		EAdType GetAdType();

		void RequestAllow(IAdRequest adRequest, Action success);
		
		bool IsAdsForbidden { get; }
	}
}