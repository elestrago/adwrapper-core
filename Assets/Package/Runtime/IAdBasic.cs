using Libs.AdWrapper.Impl;

namespace Libs.AdWrapper
{
	public interface IAdBasic
	{
		bool IsLoaded(IAdRequest adRequest);
		void Preload(IAdRequest adRequest);
		void Show(IAdRequest adRequest);
	}
}